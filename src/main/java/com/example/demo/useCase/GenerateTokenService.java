package com.example.demo.useCase;

import com.sun.security.auth.UserPrincipal;

public interface GenerateTokenService {
    String generate(String username, String password);

    UserPrincipal parseToken(String token);
}
