package com.example.demo.controller;

import com.example.demo.useCase.GenerateTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@RestController
public class WelcomeController {
    private final GenerateTokenService generateTokenService;

    @Autowired
    private AuthenticationManager authenticationManager;

    public WelcomeController(GenerateTokenService generateTokenService) {
        this.generateTokenService = generateTokenService;
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/")
    public String greeting() {
        return "Spring Security In-memory Authentication Example - Welcome " + SecurityContextHolder.getContext().getAuthentication().getName();
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping("/login")
    public String login(HttpServletResponse response, @RequestBody Map<String, String> form) throws BadCredentialsException {
        Authentication authentication  = new UsernamePasswordAuthenticationToken(form.get("username"), form.get("password"));
        SecurityContext context = SecurityContextHolder.createEmptyContext();
        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);

//        authenticationProvider.authenticate(authentication);
        authenticationManager.authenticate(authentication);
        String token = generateTokenService.generate(form.get("username"), form.get("password")).substring(7);
        Cookie cookie = new Cookie("auth_cookie", token);
        cookie.setMaxAge(60 * 60);
        cookie.setHttpOnly(true);
        response.addCookie(cookie);
        return form.get("username");
    }

}
