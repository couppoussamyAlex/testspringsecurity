package com.example.demo.security;

import com.example.demo.useCase.GenerateTokenService;
import com.sun.security.auth.UserPrincipal;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class JwtAuthorizationFilter extends OncePerRequestFilter {
    private final GenerateTokenService generateTokenService;

    public JwtAuthorizationFilter(GenerateTokenService generateTokenService) {
        this.generateTokenService = generateTokenService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String authorizationHeader = httpServletRequest.getHeader("Authorization");

        if (!authorizationHeaderIsInvalid(authorizationHeader)) {
            UsernamePasswordAuthenticationToken token = createToken(authorizationHeader);
            SecurityContextHolder.getContext().setAuthentication(token);
        }

        Cookie cookie = WebUtils.getCookie(httpServletRequest, "auth_cookie");
        if (Optional.ofNullable(cookie).isPresent()) {
            UsernamePasswordAuthenticationToken token = createToken(cookie.getValue());
            SecurityContextHolder.getContext().setAuthentication(token);
        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private boolean authorizationHeaderIsInvalid(String authorizationHeader) {
        return authorizationHeader == null
                || !authorizationHeader.startsWith("Bearer ");
    }

    private UsernamePasswordAuthenticationToken createToken(String authorizationHeader) {
        String token = authorizationHeader.replace("Bearer ", "");
        UserPrincipal userPrincipal = generateTokenService.parseToken(token);

        List<GrantedAuthority> authorities = new ArrayList<>();

        return new UsernamePasswordAuthenticationToken(userPrincipal, null, authorities);
    }
}
