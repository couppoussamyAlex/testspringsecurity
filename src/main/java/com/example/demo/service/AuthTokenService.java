package com.example.demo.service;

import com.example.demo.useCase.GenerateTokenService;
import com.sun.security.auth.UserPrincipal;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@Service
public class AuthTokenService implements GenerateTokenService {
    private String JWT_SECRET = "8LK9KfWPIrZ2tX08H8BnXH6ZETKbT+mMzuTK7w2swhc=";

    @Override
    public String generate(String username, String password) {
        Instant expirationTime = Instant.now().plus(1, ChronoUnit.HOURS);
        Date expirationDate = Date.from(expirationTime);

        Key key = Keys.hmacShaKeyFor(JWT_SECRET.getBytes());

        String compactTokenString = Jwts.builder()
                .claim("username", username)
                .setExpiration(expirationDate)
                .signWith(key, SignatureAlgorithm.HS256)
                .compact();

        return "Bearer " + compactTokenString;
    }

    @Override
    public UserPrincipal parseToken(String token) {
        byte[] secretBytes = JWT_SECRET.getBytes();
        Jws<Claims> jwsClaims = null;
        String username = "";

        try {
            jwsClaims = Jwts.parserBuilder()
                    .setSigningKey(secretBytes)
                    .build()
                    .parseClaimsJws(token);
            username = (String) jwsClaims.getBody().get("username");
        } catch (Exception ex) {
            System.out.println(ex);
        }

        return new UserPrincipal(username);
    }
}
